package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static int leftThreadsToCreateByUser = 5;
	public static int currentNumberOfIdForThreads = 0;
	public static List<Boolean> listOfStatusForThreads = new ArrayList <Boolean>();
	
	public static void main(String[] args) {
			
		List<Thread> listOfThreads = new ArrayList<Thread>(); //Lista watkow
		Scanner inputForScanner = new Scanner (System.in);
		int choiceByUser;
		int currentIndexOfThread = 0;
		
		
		System.out.println();
		System.out.println("Menu wyboru akcji do wykonania");
		System.out.println("1 == Stworzenie nowego watku");
		System.out.println("2 == Zatrzymanie watku o okreslonym id");
		System.out.println();
		
		do {
			if (leftThreadsToCreateByUser > 0) {
				System.out.println("Pozostala liczba watkow do utworzenia: "+leftThreadsToCreateByUser);
			}
			System.out.println("Uzytkowniku, co chcesz zrobic? (wybierz opcje 1 lub 2)");
			System.out.println();
			try {
				choiceByUser = inputForScanner.nextInt();
				
			} catch (Exception e) {
				System.out.println("Blad wyboru opcji watku");
				return;
			}
			switch (choiceByUser) {
				case 1:
					System.out.println();
					System.out.println("	Wybierz zadanie watku: ");
					System.out.println("	1 == Wypisywanie krotkiej wiadomosci");
					System.out.println("	2 == Krotkie odliczanie od 0 przez czas pracy watku");
					System.out.println("	3 == Wypisywanie pseudolowych liczb");
					System.out.println("	4 == Wyswietlanie procentowego zakonczenia \"pustego\" watku");
					System.out.println();
					try {
						do {
							choiceByUser = inputForScanner.nextInt();
							if (choiceByUser < 1 || choiceByUser > 4) {
								System.out.println("Brak takowego zadania. Wybierz ponownie.");
							}
						} while (choiceByUser < 1 || choiceByUser > 4);
							
					} catch (Exception e) {
						System.out.println("Blad wyboru opcji watku");
						return;
					}
					listOfStatusForThreads.add(currentIndexOfThread, true);
					listOfThreads.add(currentIndexOfThread, new Thread(new MyThread(currentIndexOfThread, choiceByUser)));
					listOfThreads.get(currentIndexOfThread).start();

					currentIndexOfThread++;
					currentNumberOfIdForThreads++;
					leftThreadsToCreateByUser--;
					break;
				case 2:
					System.out.println();
					System.out.println("Prosze podac id watku do zatrzymania:");
					try {
						choiceByUser = inputForScanner.nextInt();
					} catch (Exception e) {
						System.out.println("Nieprawidlowe id zatrzymania. Program zakonczy sie po zakonczeniu pozostalych watkow");
						return;
					}
					
					
					try {
						listOfStatusForThreads.set(choiceByUser, false);
					} catch (IndexOutOfBoundsException e) {
						System.out.println("Nie stworzono watku o id = "+choiceByUser);
					}
						

					break;
				default:
					System.out.println("Brak mozliwosci wykonania wskazanej akcji");
					//return;
			}
			
			if (leftThreadsToCreateByUser == 0) {
				System.out.println("------------------------------------------");
				System.out.println("Stworzyles juz wystarczajaca ilosc watkow.");
				System.out.println("------------------------------------------");
					
			}
		} while ( leftThreadsToCreateByUser > 0);
		
		boolean shouldStop = false;
		while (!shouldStop) {
			System.out.println("Menu wyboru akcji do wykonania");
			System.out.println("1 == Poczekanie na zakonczenie pracy watkow");
			System.out.println("2 == Zatrzymanie watku o okreslonym id");
			System.out.println("Uzytkowniku, co chcesz zrobic? (wybierz opcje 1 lub 2)");
			System.out.println();
			
			try {
				choiceByUser = inputForScanner.nextInt();
				
			} catch (Exception e) {
				System.out.println("Blad wyboru opcji watku");
				return;
			}
			
			switch (choiceByUser) {
				case 1:
					while (currentNumberOfIdForThreads > 0) {
						//Jak ktos mi powie czemu to naprawia problem petli nieskonczonej to stawiam browara!!!
						if (currentNumberOfIdForThreads == 0) {
							System.out.println();
						}
					}
					break;
				case 2:
					try {
						choiceByUser = inputForScanner.nextInt();
					} catch (Exception e) {
						System.out.println("Nieprawidlowe id zatrzymania. Program zakonczy sie po zakonczeniu pozostalych watkow");
						return;
					}
					
					try {
						listOfStatusForThreads.set(choiceByUser, false);
					} catch (IndexOutOfBoundsException e) {
						System.out.println("Nie stworzono watku o id = "+choiceByUser);
					}
					break;
				default:
					System.out.println("Brak mozliwosci wykonania wskazanej akcji");
					return;
			}
			
			if (currentNumberOfIdForThreads == 0) {
				//System.out.println();
				shouldStop=true;
			}
		}
	
		System.out.println("Zapisywanie czasow pracy watkow...");
		MyThread.charBar();
		System.out.println("Zapisano");
		
		System.out.println("Prosze nacisnac kombinacje zatrzymania (przykladowo ctrl+c) jezeli program sie nie zakonczyl...");
		return;
		
	}
}
