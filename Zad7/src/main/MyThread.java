package main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class MyThread implements Runnable {
	private int idOfThread;
	private String messageToWrite = "I'm still working...";
	private int timeMinOfRunning = 15;
	private int timeMaxOfRunning = 25;
	private int maxTimeToRandom = 5;
	private int minTimeToRandom = 2;
	private int localTemporaryForGeneratorOfTimeRunning;
	private int choiceByUser;
	static Random generatorOfNumbers = new Random();
	static List<Integer> listOfTimesForThreads = new ArrayList<Integer>();
	
	
	public MyThread (int idOfThread, int choiceByUser) {
		this.idOfThread = idOfThread;
		this.choiceByUser = choiceByUser;
	}
	
	public void run(){
		//synchronized (this) {
			switch (choiceByUser) {
			case 1:
				writeMessage();
				break;
			case 2:
				countNumber();
				break;
			case 3:
				writePseudoRandomNumbers();
				break;
			case 4:
				calculatePercentOfRealisation();
				break;
			default:
				try {
					throw new MyTaskDoNotExistException();
				} catch (Exception e) {
					System.out.println("Blad wyboru zadania");
					return;
				}
			}
		//}
	}

	
	public synchronized void  writeMessage () {
		//synchronized (this) {
			System.out.println("	Watek nr "+idOfThread+" wystartolwal!");
			int timeOfThread = generateRadnomTime(timeMinOfRunning, timeMaxOfRunning);
			int timeOfSleep;
			int localTimeOfRunning = 0;
			listOfTimesForThreads.add(idOfThread, localTimeOfRunning);
			while (timeOfThread > 0) {				
				timeOfSleep = generateRadnomTime(minTimeToRandom, maxTimeToRandom);
				if (timeOfThread - timeOfSleep < 0) {
					timeOfSleep = timeOfThread;
				}
				localTimeOfRunning += timeOfSleep;
				listOfTimesForThreads.set(idOfThread, localTimeOfRunning);
				try {
					Thread.sleep(timeOfSleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (Main.listOfStatusForThreads.get(idOfThread)==false){
					try {
						System.out.println("	Watek (id="+idOfThread+") zostal zatrzymany");
						Main.currentNumberOfIdForThreads--;
						wait();
					} catch (Exception e) {
						System.out.println("Blad zatrzymania watku");
					}
				}
				//Main.listOfStatusForThreads.set(idOfThread, true);
				
				timeOfThread -=timeOfSleep;
				System.out.println("	Watek (id="+idOfThread+"): "+messageToWrite);
	
			}
			System.out.println("	Watek nr "+idOfThread+" zostal zakonczony!");
			Main.currentNumberOfIdForThreads--;

	}
	
	public synchronized void countNumber() {
		//synchronized (this) {
			System.out.println("	Watek nr "+idOfThread+" wystartolwal!");
			int timeOfThread = generateRadnomTime(timeMinOfRunning, timeMaxOfRunning);
			int timeOfSleep;
			int localTimeOfRunning = 0;
			listOfTimesForThreads.add(idOfThread, localTimeOfRunning);
			int counter = 1;
			while (timeOfThread > 0) {
				timeOfSleep = generateRadnomTime(minTimeToRandom, maxTimeToRandom);
				if (timeOfThread - timeOfSleep < 0) {
					timeOfSleep = timeOfThread;
				}
				localTimeOfRunning += timeOfSleep;
				listOfTimesForThreads.set(idOfThread, localTimeOfRunning);
				try {
					Thread.sleep(timeOfSleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (Main.listOfStatusForThreads.get(idOfThread)==false){
					try {
						System.out.println("	Watek (id="+idOfThread+") zostal zatrzymany");
						Main.currentNumberOfIdForThreads--;
						wait();
					} catch (Exception e) {
						System.out.println("Blad zatrzymania watku (id="+idOfThread+")");
					}
				}
				
				timeOfThread -=timeOfSleep;
				System.out.println("	Watek (id="+idOfThread+"): "+counter);
				counter++;
			}
			System.out.println("	Watek nr "+idOfThread+" zostal zakonczony!");
			Main.currentNumberOfIdForThreads--;
		//}
	}


	public synchronized void writePseudoRandomNumbers () {
		//synchronized (this) {
			System.out.println("	Watek nr "+idOfThread+" wystartolwal!");
			int timeOfThread = generateRadnomTime(timeMinOfRunning, timeMaxOfRunning);
			int timeOfSleep;
			int localTimeOfRunning = 0;
			listOfTimesForThreads.add(idOfThread, localTimeOfRunning);
			while (timeOfThread > 0) {
				timeOfSleep = generateRadnomTime(minTimeToRandom, maxTimeToRandom);
				if (timeOfThread - timeOfSleep < 0) {
					timeOfSleep = timeOfThread;
				}
				localTimeOfRunning += timeOfSleep;
				listOfTimesForThreads.set(idOfThread, localTimeOfRunning);
				try {
					Thread.sleep(timeOfSleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (Main.listOfStatusForThreads.get(idOfThread)==false){
					try {
						System.out.println("	Watek (id="+idOfThread+") zostal zatrzymany");
						Main.currentNumberOfIdForThreads--;
						wait();
					} catch (Exception e) {
						System.out.println("Blad zatrzymania watku (id="+idOfThread+")");
					}
				}
				
				timeOfThread -=timeOfSleep;
				System.out.println("	Watek (id="+idOfThread+"): "+generatorOfNumbers.nextInt());
			}
			System.out.println("	Watek nr "+idOfThread+" zostal zakonczony!");
			Main.currentNumberOfIdForThreads--;
		//}
	}
		
	public synchronized void calculatePercentOfRealisation () {
		//synchronized (this) {
			System.out.println("	Watek nr "+idOfThread+" wystartolwal!");
			String progressForRealisation= "";
			int timeOfThread = generateRadnomTime(timeMinOfRunning, timeMaxOfRunning);
			int temporaryTimeOfThread = timeOfThread;
			int timeOfSleep;
			int localTimeOfRunning = 0;
			listOfTimesForThreads.add(idOfThread, localTimeOfRunning);
			double percentOFDone;
			while (timeOfThread > 0) {
				timeOfSleep = generateRadnomTime(minTimeToRandom, maxTimeToRandom);
				if (timeOfThread - timeOfSleep < 0) {
					timeOfSleep = timeOfThread;
				}
				localTimeOfRunning += timeOfSleep;
				listOfTimesForThreads.set(idOfThread, localTimeOfRunning);
				try {
					Thread.sleep(timeOfSleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (Main.listOfStatusForThreads.get(idOfThread)==false){
					try {
						System.out.println("	Watek (id="+idOfThread+") zostal zatrzymany");
						Main.currentNumberOfIdForThreads--;
						wait();
					} catch (Exception e) {
						System.out.println("Blad zatrzymania watku (id="+idOfThread+")");
					}
				}
				
				timeOfThread -=timeOfSleep;
				progressForRealisation = "";
				percentOFDone = (temporaryTimeOfThread - timeOfThread)/(double)temporaryTimeOfThread * 100;
				for (int i = 0; i < (int)percentOFDone/10; i++) {
					progressForRealisation+="||";
				}
				System.out.println("	Watek (id="+idOfThread+"):"+progressForRealisation+" "+percentOFDone+"%");
			}
			System.out.println("	Watek nr "+idOfThread+" zostal zakonczony!");
			Main.currentNumberOfIdForThreads--;
		//}
	}
	
	public int generateRadnomTime(int min, int max) {
		int random = generatorOfNumbers.nextInt();
		if (random < 0) {
			random *= -1;
		}
		random = min + random %(max - min);
		random *= 1000  ; //liczba milisekund do uspienia
		random += Math.abs(generatorOfNumbers.nextInt()%1000);
		return random;
	}


	static void charBar(){
		// Dane
		int counterForSavingCharBar = 0;
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		for (Integer x : listOfTimesForThreads) {
			dataset.setValue(x, "Czas Dzialania", ""+counterForSavingCharBar);
			counterForSavingCharBar++;
		}
		
		// Tworzy wykres typu Bar - s�upkowy
		JFreeChart chart = ChartFactory.createBarChart("Wykres czasow prac watkow",
			"X - Numer watku (id)", "Y - Czas dzialania (sekundy)", dataset, PlotOrientation.VERTICAL,
			false, true, false);

		try {
			final ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
			final File file1 = new File ("Wykres.png");
			ChartUtilities.saveChartAsPNG(file1, chart, 600, 400, info);
		}	catch (Exception e) {
			System.out.println("Blad zapisu wykresu");
			return;
		}
		return;
		
	}
	
//	public void myWay () {
//		synchronized (this) {
//			try {
//				this.wait();
//			} catch (InterruptedException e) {
//				return;
//			}
//		}
//	}
	
}
